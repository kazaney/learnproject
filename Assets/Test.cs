﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour
{
	public GameObject Pref;

	public int size = 4;

	void Start ()
	{
		//cols
		for(int i=0;i<size;i++)
		{
			//rows
			for(int j=0;j<size;j++)
			{
				if(j%2 == i % 2)
				{
					continue;
				}

				Vector3 pos = new Vector3(i,j,0);

				Instantiate(Pref, pos, Quaternion.identity);
			}
		}
	}
}